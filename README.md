# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This a simple e-commerce project that I have tried to create my own MVC structure from scratch. 
I have used php-activerecord to create and handle models, predis library for data structure storage, simple unit testing with php-unit.

### How do I get set up? ###

* set database from core/config.php
* install redis-server to your machine
* call composer install in your root directory
* test using php-unit

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* Repo owner or admin

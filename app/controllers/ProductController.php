<?php
/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 4/28/17
 * Time: 5:26 PM
 */


use core\controllers\Controller;
use core\Application;
use app\models\Product;

class ProductController extends Controller
{
    public function index(){
        $this->render('site/index',[
            'name'=>'Jumshud',
            'age'=>75
        ]);
    }

    public function create(){
        header('Content-Type: application/json');

        if (Application::$request->isPost() && Application::$request->isAjax()) {
            $data = json_decode($_POST);

            //create product
            $model = new Product();
            $model->name = $data['name'];
            $model->category_id = is_numeric($data['category_id'])?$data['category_id'] : 0;

            if($model->save()){
                $model->writeToRedis();
                echo json_encode([
                    'success' => 1,
                    'message' => 'Product successfully created.'
                ]);
            } else {
                echo json_encode([
                    'success' => 1,
                    'message' => $model->errors
                ]);
            }

        } else {
            echo 'invalid request';
        }
    }

    public function createManual(){
        header('Content-Type: application/json');

        if (Application::$request->isPost() && Application::$request->isAjax()) {
            $data = json_decode($_POST);

            //prepere query
            $db = \core\Database::getInstance();
            $db->query('INSERT INTO product (name, code, category1_id) VALUES (:name, :code, :category1_id)');

            //create product
            $model = new Product();

            $db->bind(':name', $data['name']);
            $db->bind(':code', $data['code']);
            $db->bind(':category1_id', $data['category1_id'], PDO::PARAM_INT);

            if($db->execute()){
                echo json_encode([
                    'success' => 1,
                    'message' => 'Product successfully created.'
                ]);
            } else {
                echo json_encode([
                    'success' => 1,
                    'message' => $model->errors
                ]);
            }

        } else {
            echo 'invalid request';
        }
    }
}
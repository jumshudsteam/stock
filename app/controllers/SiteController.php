<?php
/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 4/28/17
 * Time: 5:26 PM
 */


use core\controllers\Controller;

class SiteController extends Controller
{
    public function index(){
        $this->render('site/index',[
            'name'=>'Jumshud',
            'age'=>75
        ]);
    }

    public function contact(){
        $this->render('site/contact',[
            'name'=>'Jumshud',
            'email'=> 'cumshud.mecidli@gmail.com'
        ]);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 4/28/17
 * Time: 5:17 PM
 */

require_once dirname(__DIR__).'/vendor/autoload.php';
require_once dirname(__DIR__).'/app/core/config.php';
require_once dirname(__DIR__).'/app/core/Application.php';
require_once dirname(__DIR__).'/app/core/Request.php';
require_once dirname(__DIR__).'/app/core/Controller.php';
require_once dirname(__DIR__).'/app/core/Database.php';
require_once dirname(__DIR__).'/app/core/ModelInterface.php';
require_once dirname(__DIR__).'/app/core/BaseModel.php';
require_once dirname(__DIR__).'/app/core/Redis.php';
require_once dirname(__DIR__).'/app/core/Autoloader.php';

<?php
/**
 * Created by PhpStorm.
 * User: Cumshud
 * Date: 4/28/2017
 * Time: 9:07 PM
 */

namespace app\models;


use core\models\BaseModel;

class Product extends BaseModel
{
    const STATUS_ACTIVE = 10;

    static $table_name = 'product';

    static $validates_presence_of = array(
        array('name'),
        array('code'),
        array('category1_id'),
    );

    static $validates_numericality_of = array(
        array('category1_id', 'only_integer' => true),
        array('category2_id', 'only_integer' => true),
        array('category3_id', 'only_integer' => true),
        array('color_id', 'only_integer' => true),
        array('buy_price', 'only_integer' => true),
        array('measure_id', 'only_integer' => true),
        array('material_id', 'only_integer' => true),
    );

    public $name;
    public $code;
    public $category1_id;
    public $category2_id;
    public $category3_id;
    public $color_id;
    public $material_id;
    public $buy_price;
    public $measure_id;

    public static function firstProduct()
    {
        return 'Apple Iphone 7';
    }

    /**
     * add product to redis
     * @return void
     */
    public function writeToRedis()
    {
        $redis = \core\Application::$redis;
        $redis->sadd('product_ids', $this->id);
        $redis->hset('product_0','name', $this->name);
        $redis->hset('product_0','category', $this->category3_id);
        $redis->hset('product_0','price', $this->buy_price);
    }

}
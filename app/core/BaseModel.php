<?php
/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 4/28/17
 * Time: 5:20 PM
 */

namespace core\models;

use ActiveRecord;

abstract class BaseModel extends ActiveRecord\Model implements ModelInterface
{
    public function __construct(){
        parent::__construct();
        $connections = array(
            'development' => 'mysql://anbarinetlab:h%&Wl4d}cI@localhost/anbarine_db',
            'production' => 'mysql://username:password@localhost/production',
            'test' => 'mysql://username:password@localhost/test'
         );

        # must issue a "use" statement in your closure if passing variables
        ActiveRecord\Config::initialize(function($cfg) use ($connections)
        {
            $cfg->set_model_directory('models');
            $cfg->set_connections($connections);

            #default connection is now development
            $cfg->set_default_connection('development');
        });
    }
}
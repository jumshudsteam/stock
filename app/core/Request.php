<?php
/**
 * Created by PhpStorm.
 * User: Cumshud
 * Date: 4/28/2017
 * Time: 9:55 PM
 */

namespace core;


class Request
{
    public function isPost(){
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }

    public function isAjax(){
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 4/28/17
 * Time: 5:17 PM
 */

namespace core;


class Application
{
    protected $controller = 'SiteController';
    protected $action = 'index';
    protected $params = array();
    public static $request;
    public static $redis;


    public function __construct(){
        $url = $this->parseUrl();
        self::$request = new Request();
        self::$redis = Redis::getInstance();

        if(!empty($url[0])) {
            $this->controller = $this->getControllerClass($url[0]);
        }
        $dirName = dirname(__DIR__).'/controllers/'. $this->controller .'.php';

        if(file_exists($dirName)){
            require_once $dirName;
            unset($url[0]);
        } else {
            throw new \Exception($this->controller . ' class not found.');
        }


        if(!empty($url[1])) {
            $this->action = $this->dashToCamelCase($url[1]);
            unset($url[1]);
            if(method_exists($this->controller, $this->action)) {

            } else {
                throw new \Exception("$this->action method doesn't exist in $this->controller class.");
            }
        }

        $this->params = $url ? array_values($url) : array();

    }

    /**
     * expolode string to array from string
     * @return array
     */
    private function parseUrl()
    {
        if(isset($_GET['url'])) {
            return $url = explode('/',filter_var(rtrim($_GET['url'],'/'),FILTER_SANITIZE_URL));
        }
    }

    /**
     * convert dahs-case string to CamelCase eg. about->us => AboutUs
     * @param $str string input string
     * @return string controller class name
     */
    private function dashToCamelCase($str) {
        return implode('', array_map(function($el){ return ucfirst($el);}, explode('-', $str)));
    }

    /**
     *Find controller class from user request string
     * @param $str string input string
     * @return string controller class name
     */
    private function getControllerClass($str) {
        return $this->dashToCamelCase($str).'Controller';
    }

    public function run(){
        call_user_func_array(array(new $this->controller, $this->action),$this->params);
    }
}
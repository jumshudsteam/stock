<?php
/**
 * Created by PhpStorm.
 * User: Cumshud
 * Date: 4/28/2017
 * Time: 11:10 PM
 */

namespace core;

use Predis\Client;

class Redis
{
    private static $host      = '127.0.0.1';
    private static $port      = 6379;
    private static $scheme      = 'tcp';

    private static $instance = null;

    private function __construct(){

    }

    protected function __clone()
    {
        //Me not like clones! Me smash clones!
    }
    protected function __wakeup()
    {
        //don't serialize me
    }

    public static function getInstance()
    {
        if (!isset(static::$instance)) {
            self::$instance = new Client([
                "scheme" => self::$scheme,
                "host" => self::$host,
                "port" => self::$port,
                "persistent" => "1"
            ]);
        }
        return self::$instance;
    }
}
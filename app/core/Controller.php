<?php
/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 4/28/17
 * Time: 5:17 PM
 */
namespace core\controllers;

abstract class Controller
{
    public function render($view, $params=[]){
        extract($params);
        require_once '../app/views/' . $view . '.php';
    }

}
<?php

/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 6/13/17
 * Time: 7:04 PM
 */
class Autoloader {
    static public function loader($className) {
        $filename = dirname(dirname(__DIR__)). '/' . str_replace("\\", '/', $className) . ".php";
        if (file_exists($filename)) {
            include($filename);
            if (class_exists($className)) {
                return TRUE;
            }
        }
        return FALSE;
    }
}
spl_autoload_register('Autoloader::loader');
<?php

/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 5/24/17
 * Time: 1:50 AM
 */

use PHPUnit\Framework\TestCase;
use app\models\Product;

class ProductTest extends TestCase
{
    public function testFirstProduct()
    {
        $this->assertEquals('Apple Iphone 7', Product::firstProduct());
    }

    public function testCheckStatus()
    {
        $this->assertTrue(Product::STATUS_ACTIVE == 10);
    }
}
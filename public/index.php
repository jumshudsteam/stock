<?php
/**
 * Created by PhpStorm.
 * User: jumshud
 * Date: 4/28/17
 * Time: 5:09 PM
 */

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);

require_once '../app/init.php';

$app = new \core\Application();
$app->run();
